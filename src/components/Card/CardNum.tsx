import * as React from 'react';
import styled from 'styled-components';

type NumProps = {
  num?: string;
  color: string;
};

const Num = styled.div`
  color: ${(props: NumProps) => props.color};
  font: 18px arial;
  font-weight: bold;
  line-height: 172%;
  text-indent: 4px;
`;

const CardNum = ({ num, color }: NumProps) => {
  return <Num color={color}>{num}</Num>;
};

export default CardNum;
