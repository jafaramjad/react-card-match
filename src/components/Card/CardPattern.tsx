import * as React from 'react';
import styled from 'styled-components';

const PatternShell = styled.div`
  font-size: 83px;
  line-height: 96%;
  text-align: center;
  color: #dcdcdc;
`;

const CardPattern = () => {
  return <PatternShell />;
};
// &#9641;
// &#9641;
// &sect;
// content: "&sect;";
export default CardPattern;
