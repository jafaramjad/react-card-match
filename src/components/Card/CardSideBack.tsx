import * as React from 'react';
import styled from 'styled-components';

import CardPattern from './CardPattern';

const BackContainer = styled.div`
  backface-visibility: hidden;
  background: #999;
  position: absolute;
  top: 0px;
  left: 0px;
  height: 100%;
  width: 100%;
  padding: 0px;
  user-select: none;
  display: ${(props: BackProps) => (props.visible ? 'block' : 'none')};
`;

type BackProps = {
  visible: boolean;
  className?: string;
};

const CardSideBack = ({ visible }: BackProps) => {
  return (
    <BackContainer visible={visible}>
      <CardPattern />
    </BackContainer>
  );
};

export default CardSideBack;
