import * as React from 'react';
import styled from 'styled-components';

import CardSuit from './CardSuit';
import CardNum from './CardNum';

const FrontContainer = styled.div`
  background: #fff;
  position: absolute;
  top: 0px;
  left: 0px;
  height: 100%;
  width: 100%;
  user-select: none;
  display: ${(props: FrontContainerProps) =>
    props.visible ? 'block' : 'none'};
`;

export interface FrontInterface {
  Props: FrontProps;
}

type FrontContainerProps = {
  visible: boolean;
};

type FrontProps = {
  className?: string;
  visible: boolean;
  num: string;
  suit: string;
  color: string;
};

const CardSideFront = ({ visible, suit, num, color }: FrontProps) => {
  return (
    <FrontContainer visible={visible}>
      <CardNum num={num} color={color} />
      <CardSuit suit={suit} color={color} />
    </FrontContainer>
  );
};

export default CardSideFront;
