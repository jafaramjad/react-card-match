import * as React from 'react';
import styled from 'styled-components';

type SuitProps = {
  suit?: any | string;
  color: string;
};

const Suit = styled.div`
  color: ${(props: SuitProps) => props.color};
  font-size: 42px;
  text-align: center;
`;

const CardSuit = ({ color, suit }: SuitProps) => {
  return <Suit color={color}>{suit}</Suit>;
};

export default CardSuit;
