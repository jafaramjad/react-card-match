import * as React from 'react';
import styled from 'styled-components';

import Card from '../Card/Card';
// import XCard from '../../containers/XCard';
import { DeckState, DeckProps, CardProps } from '../../models';
import * as DeckFunc from './DeckFunc';

const DeckShell = styled.div``;

class Deck extends React.Component<DeckProps, DeckState> {
  constructor(props: DeckProps) {
    super(props);

    this.state = {
      shuffled: false,
      dealt: false,
      finished: false,
      deck: DeckFunc.createDeck(),
      flipped: []
    };
  }

  /** 
   * Update the list of cards clicked, either add or remove
   */
  updateList = (state: DeckState, cid: string): Array<string> => {
      // IF NOT FOUND IN LIST
      return !state.flipped.find(x => x === cid) 
        ? [...state.flipped, cid] // ADD
        : state.flipped.filter(x => x !== cid); // REMOVE
  }

  /** 
   * Update the deck with the new list of cards clicked
   */
  updateDeck = (state: DeckState, newList: Array<string>): Array<CardProps> => {
    return state.deck.map((card: CardProps) => {
      card.flipped = newList.find(x => x === card.cid) 
        ? true 
        : false;
      return card;
    });
  }

  checkCard = (state: DeckState, cid: string): string => {
    let list = state.flipped;
    let listLength = state.flipped.length;

    // CHECK 1 - How many have been clicked? Only two max
    let isLengthGood = listLength <= 1 ? true : false;

    // CHECK 2 - Was same card clicked with only 1 (length) in list
    let isNotSameCard = listLength === 1
      ? list.find(x => x === cid) 
        ? true 
        : false
      : false;

    // CHECK 3 - After 2 are clicked, do they match? Then flip back over
    let isTwoClicked = listLength === 2
      ? true
      : false;

    let message: string;

    // tslint:disable-next-line:no-console
    console.log(`
      List: ${list}
      Length: ${listLength}
    `);

    switch (false) {
      case isLengthGood:
        message = 'Length no good';
        break;
      case !isNotSameCard:
        message = 'Is same card';
        break;
      case !isTwoClicked:
        message = 'Two cards have been clicked on';
        break;
      default:
        message = 'Good To Go';
        break;
    }

    return message;

    // return isLengthGood && isNotSameCard && isTwoClicked ? true : false ;
  }
  /** 
   * An event handler for clicking a card
   * @name handleCardClick
   * @param {string} cid - Card ID
   */
  handleCardClick = (cid: string) => {

    let checkCardStatus = this.checkCard(this.state, cid);
    // tslint:disable-next-line:no-console
    console.log(checkCardStatus);

    if (checkCardStatus !== 'Good To Go' ) {
      return;
    }

    // UPDATE LIST
    let updatedList: Array<string> = this.updateList(this.state, cid);

    // UPDATE DECK
    let updatedDeck: Array<CardProps> = this.updateDeck(this.state, updatedList);

    // tslint:disable-next-line:no-console
    console.log(`
      ID: ${cid}
      Length: ${updatedList.length}
      Cards: ${updatedList.length > 0 ? updatedList : 'none'}
    `);

    this.setState({
      deck: updatedDeck,
      flipped: updatedList
    });
  }

  public render() {
    return (
      <DeckShell>
        {this.state.deck.map((card: CardProps) => {
          return (
            <Card
              key={card.cid}
              cid={card.cid}
              num={card.num}
              suit={card.suit}
              color={card.color}
              sym={card.sym}
              flipped={card.flipped}
              handleClick={this.handleCardClick}
            />
          );
        })}
      </DeckShell>
    );
  }
}

export default Deck;
