import * as React from 'react';
import styled from '../../theme';
import { Col } from 'react-bootstrap';

import Table from '../Table/Table';

const RoomBG = styled.div`
  color: brown;
  background-color: #ccc;
  top: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  background: linear-gradient(to top, #dcdcdc, #fff);
`;

const RoomTitle = styled(Col)`
  font-family: arial;
  text-align: center;
  background: #fff;
  color: black;
`;

interface RoomProps {}
interface RoomState {}

class Room extends React.Component<RoomProps, RoomState> {
  constructor(props: RoomProps) {
    super(props);
  }

  public render() {
    return (
      <RoomBG>
        <RoomTitle lg={12} md={12} sm={12} xs={24}>
          <h1>Match</h1>
        </RoomTitle>
        <Table game={'Match'} players={2} />
      </RoomBG>
    );
  }
}

export default Room;
