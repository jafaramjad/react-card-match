import * as React from 'react';
import styled from 'styled-components';

import Match from '../Game/Match/Match';

const TableBG = styled.div`
  background-color: #0f0;
`;

interface TableProps {
  game: 'Match' | 'BlackJack' | 'TexasHoldem';
  players: number;
  playerNames?: [string];
  someDefaultValue?: string;
}

interface TableState {
  players: number;
}

class Table extends React.Component<TableProps, TableState> {
  constructor(props: TableProps) {
    super(props);

    this.state = {
      players: this.props.players
    };
  }

  getGame(gameName: string) {
    if (gameName === 'Match') {
      return <Match />;
    } else {
      return 'Coming Soon!';
    }
  }

  render() {
    return (
      <TableBG>
        <div>{this.getGame(this.props.game)}</div>
      </TableBG>
    );
  }
}

export default Table;
