import * as React from 'react';
import styled from '../../theme';

interface Props {
  playerAmountProps: number;
}

interface State {
  playerAmount: number;
}

const WelcomeShell = styled.div``;

class Welcome extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  public render() {
    return (
      <WelcomeShell>
        <h1>Welcome!</h1>
      </WelcomeShell>
    );
  }
}

export default Welcome;
