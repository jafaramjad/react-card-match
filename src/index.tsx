import * as React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import { reducers } from './store';
import Room from './components/Room/Room';

const store = createStore(reducers);

const App = () => (
  <Provider store={store}>
    <Room />
  </Provider>
);

render(<App />, document.getElementById('root'));
