/**
 * CARD ******************************
 *   //clickCard: (uid: string) => void;
 */
export interface CardProps {
  key: string;
  cid: string;
  num: string;
  suit: string;
  color: string;
  sym: string;
  flipped: boolean;
  handleClick: any;
}

export interface CardState {
  frontVisible: boolean;
  backVisible: boolean;
  cid: string;
  isFlipped: boolean;
}

/**
 * DECK ******************************
 */
export interface DeckState {
  shuffled: boolean;
  dealt: boolean;
  finished: boolean;
  deck: Array<CardProps>;
  flipped: Array<string>;
}

export interface DeckProps {}
