import { ActionCreator } from 'redux';
import { CardClickType } from './types';

export const CardClickAction: ActionCreator<CardClickType> = (cid: string) => ({
  type: 'CARD_CLICK',
  payload: {
    cid: 'AAA'
  }
});
