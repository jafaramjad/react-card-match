import { Reducer } from 'redux';
import { CardClickType } from './types';

// import {CardClickAction} from "./actions";

export const initialState: CardClickType = {
  type: 'CARD_CLICK',
  payload: {
    cid: ''
  }
};

const cardsReducer: Reducer<CardClickType> = (
  state: CardClickType = initialState,
  action
) => {
  switch (action.type) {
    case 'CARD_CLICK':
      // console.log('card was clicked...reduce...');
      return {
        ...state,
        payload: {
          cid: action.payload.cid
        }
      };
    default:
      return state;
  }
};

// const cardsReducer = (state: any, action: any) => {
//   console.log(action.type);
//   switch (action.type) {
//     case "CLICK_CARD":
//       return {};
//     default:
//       return {};
//   }
// };

export default cardsReducer;
